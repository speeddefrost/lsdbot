#include "lsdbot.h"

#include <string.h>

struct irc_user *
irc_user_new (char *name, char *nick, char *host) {
    int e;
    struct irc_user *u;

    u = malloc(sizeof(struct irc_user));
    if(u == NULL)
        return NULL;

    u->name = NULL;
    u->nick = NULL;
    u->ident = NULL;
    u->host = NULL;
    u->access = -1;

    e = map_init(&u->data, 8, 8);
    if(e != 0)
        goto e1;

    e = map_init(&u->flags, 16, 8);
    if(e != 0)
        goto e2;

    e = irc_user_update(u, name, nick, host);
    if(e != 0)
        goto e3;

    return u;

e3: map_deinit(&u->flags);
e2: map_deinit(&u->data);
e1: free(u);

    return NULL;
}

void irc_user_destroy (struct irc_user *u) {
    free(u->name);
    free(u->nick);
    free(u->ident);
    free(u->host);
    map_deinit(&u->data);
    map_deinit(&u->flags);
    free(u);
}

char * irc_user_get_mask (struct irc_user *u, bool nick, bool ident, bool host) {
    size_t size;
    char *n,
         *i,
         *h,
         *mask;

    size = 3;

    if(nick) {
        n = u->nick;
        size += strlen(u->nick);
    }
    else {
        n = "*";
        size += 1;
    }

    if(ident) {
        i = u->ident;
        size += strlen(u->ident);
    }
    else {
        i = "*";
        size += 1;
    }

    if(host) {
        h = u->host;
        size += strlen(u->host);
    }
    else {
        h = "*";
        size += 1;
    }

    mask = malloc(size);
    if(mask == NULL)
        return NULL;

    snprintf(mask, size, "%s!%s@%s", n, i, h);
    return mask;
}

int irc_user_set_data (struct irc_user *u, int type, char *key, void *data) {
    int *i;
    unsigned int *ui;
    double *f;
    char *s;
    void *v;

    if(type == DATA_INT) {
        i = malloc(sizeof(int));
        if(i == NULL)
            return -1;

        memcpy(i, data, sizeof(int));
        map_insert(&u->data, key, data, free);
    }
    else if(type == DATA_UINT) {
        ui = malloc(sizeof(unsigned int));
        if(ui == NULL)
            return -1;

        memcpy(i, data, sizeof(unsigned int));
        map_insert(&u->data, key, data, free);
    }
    else if(type == DATA_FLOAT) {
        f = malloc(sizeof(double));
        if(f == NULL)
            return -1;

        memcpy(i, data, sizeof(double));
        map_insert(&u->data, key, data, free);
    }
    else if(type == DATA_STR) {
        s = strdup(data);
        if(s == NULL)
            return -1;

        map_insert(&u->data, key, s, free);
    }
    else {
        map_insert(&u->data, key, data, NULL);
    }

    return 0;
}

int irc_user_update (struct irc_user *u, char *name, char *nick, char *host) {
    if(name != NULL) {
        free(u->name);
        u->name = strdup(name);
        if(u->name == NULL)
            return -1;
    }

    if(nick != NULL) {
        free(u->nick);
        u->nick = strdup(nick);
        if(u->nick == NULL)
            return -1;
    }

    if(host != NULL) {
        free(u->host);
        u->host = strdup(host);
        if(u->host == NULL)
            return -1;
    }

    return 0;
}

int irc_user_update_flags (struct irc_user *u, char *channel, uint8_t mask, uint8_t flags) {
    uint8_t *f;
    int e;

    f = map_get(&u->flags, channel);
    if(f == NULL) {
        f = malloc(sizeof(uint8_t));
        if(f == NULL)
            return -1;

        e = map_insert(&u->flags, channel, f, free);
        if(e != 0) {
            free(f);
            return e;
        }
    }

//       preserved      new
    *f = (*f & ~mask) | (mask & flags);
    return 0;
}
