#include "lsdbot.h"

#include <clip/string.h>
#include <clip/term_colours.h>
#include <string.h>

void irc_event_clear (struct irc_event *ev) {
    free(ev->nick);
    free(ev->ident);
    free(ev->host);
    free(ev->message);
    strbuf_free(ev->argv, ev->argc);
    memset(ev, 0, sizeof(struct irc_event));
}

int irc_event_process (struct irc_event *ev) {
    size_t i;
    struct irc_listener *l;
    int e;

    list_for_each(&ev->conn->bot->listeners, i,l) {
        if((l->conn   == NULL || l->conn == ev->conn) &&
           (l->action == NULL || strcmp(ev->action, l->action) == 0))
        {
            e = l->callback(ev);
            if(e != 0)
                debug("[%s/%s] listener error: %i\n", l->conn != NULL ? l->conn->name : "*", l->action != NULL ? l->action : "*", e);

            if(l->tmp) {
                debug("[%s/%s] removing temporary listener\n", l->conn != NULL ? l->conn->name : "*", l->action != NULL ? l->action : "*");
                list_remove(&ev->conn->bot->listeners, i--);
            }
        }
    }

    return 0;
}
