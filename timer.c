#include "lsdbot.h"

struct irc_timer *
irc_timer_new (struct irc_conn *c, char *timeout_desc, irc_timer_callback *callback, void *data) {
    unsigned long long v;
    char *spec;
    struct irc_timer *t;
    int e;

    if(c == NULL)
        return NULL;

    v = strtoull(timeout_desc, &spec, 0);
    if(v == 0)
        return NULL;

    if(*spec == 'm')
        v *= 60;
    else if(*spec == 'h')
        v *= 3600;
    else if(*spec == 'd')
        v *= 86400;

    t = malloc(sizeof(struct irc_timer));
    if(t == NULL)
        return NULL;

    e = list_append(&c->bot->timers, t, irc_timer_destroy);
    if(e != 0) {
        free(t);
        return NULL;
    }

    t->conn = c;
    t->active = false;
    t->timeout = v;
    t->cutoff = 0;
    t->callback = callback;
    t->data = data;

    return t;
}

void irc_timer_destroy (struct irc_timer *t) {
    list_remove_data(&t->conn->bot->timers, t);
    irc_timer_stop(t);
    free(t);
}

void irc_timer_start (struct irc_timer *t) {
    if(t->active)
        return;

    t->cutoff = time(NULL) + t->timeout;
    t->active = true;
}

void irc_timer_stop (struct irc_timer *t) {
    t->active = false;
    t->cutoff = 0;
}
