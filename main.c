#include <lsdbot.h>

#include <clip/string.h>

int main (int argc, char **argv) {
    struct irc_bot b;
    size_t i,j;
    char **fdinfo;
    size_t fdinfo_count;
    struct irc_conn *c;

    lsdbot_rng.state = time(NULL);

    irc_bot_init(&b);
    if(irc_bot_load_config(&b, "config.json") != 0)
        return 1;

//  restore connection file descriptors that were passed with execv()
//  -- refer to cmd_reload() in core.c
    for(i=1; i < argc; ++i) {
        fdinfo = strsplit(argv[i], ":", &fdinfo_count);
        if(fdinfo == NULL)
            continue;

        c = map_get(&b.conns, fdinfo[0]);
        if(c != NULL) {
            c->fd = strtoull(fdinfo[1], NULL, 10);
            irc_names(c, NULL);
        }

        strbuf_free(fdinfo, fdinfo_count);
    }

    irc_bot_start(&b);
    irc_bot_wait(&b);
    irc_bot_deinit(&b);

    return 0;
}
