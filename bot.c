#include "lsdbot.h"

#include <clip/filesystem.h>
#include <clip/string.h>
#include <string.h>

// threads.c
extern void * conn_thread (void *arg);
extern void * timer_thread (void *);

void listener_destroy (struct irc_listener *l) {
    free(l->action);
    free(l);
}

inline static
int load_connections (struct irc_bot *bot, struct tree_node *n) {
    int e;
    struct irc_conn *c;

    int port;
    char *host,
         *password,
         *nick,
         *user,
         *real_name,
         *quit_msg,
         *nickserv,
         *nickserv_password;

    bool has_admin;

    struct tree_node *a,
                     *ac;

    struct irc_user *u;
    struct irc_channel *ch;

    char **ch_argv;
    size_t ch_argc;

    for(; n != NULL; n = n->next) {
        if(n->name == NULL || n->type != TREE_NODE_DICT)
            continue;

        debug("loading network: %s ...\n", n->name);

        tree_get(n, "host", TREE_NODE_STR, &host);
        if(host == NULL) {
            debug("  error: no host configured\n");
            continue;
        }
        else {
            debug("  host: %s\n", host);
        }

        tree_get(n, "nick", TREE_NODE_STR, &nick);
        if(nick == NULL) {
            if(bot->defaults.nick != NULL) {
                nick = bot->defaults.nick;
                debug("  nick: default (%s)\n", nick);
            }
            else {
                debug("  error: no nick configured\n");
                continue;
            }
        }
        else {
            debug("  nick: %s\n", nick);
        }

        tree_get(n, "user", TREE_NODE_STR, &user);
        if(user == NULL) {
            if(bot->defaults.user != NULL) {
                user = bot->defaults.user;
                debug("  user: default (%s)\n", user);
            }
            else {
                debug("  error: no user configured\n");
                continue;
            }
        }
        else {
            debug("  user: %s\n", user);
        }

        tree_get(n, "port", TREE_NODE_INT, &port);
        tree_get(n, "password", TREE_NODE_STR, &password);
        tree_get(n, "real_name", TREE_NODE_STR, &real_name);
        tree_get(n, "quit_msg", TREE_NODE_STR, &quit_msg);
        tree_get(n, "nickserv", TREE_NODE_STR, &nickserv);
        tree_get(n, "nickserv_password", TREE_NODE_STR, &nickserv_password);

        if(real_name == NULL) {
            real_name = bot->defaults.real_name;
            debug("  real name: default (%s)\n", real_name != NULL ? real_name : "<null>");
        }
        else {
            debug("  real name: %s\n", real_name);
        }

        if(quit_msg == NULL) {
            quit_msg = bot->defaults.quit_msg;
            debug("  quit message: default (%s)\n", quit_msg != NULL ? quit_msg : "<null>");
        }
        else {
            debug("  quit message: %s\n", quit_msg);
        }

        e = irc_conn_new(&c, n->name, bot);
        if(e != 0)
            return e;

        e = irc_conn_configure(c, host, port, password, nick, user, real_name, quit_msg, nickserv, nickserv_password);
        if(e != 0)
            goto error;

    //  load acl
        if(tree_descend(n, "access", TREE_NODE_DICT, false, &a) != 0) {
            debug("  error: no access list configured\n");
            goto error;
        }

        debug("  acl:\n");

        has_admin = false;
        tree_node_for_each(a,a) {
            if(a->type != TREE_NODE_INT || a->name == NULL)
                continue;

            e = irc_conn_acl_add(c, a->name, a->data.i);
            if(e != 0)
                goto error;

            if(a->data.i == 10)
                has_admin = true;

            debug("    %s [%lli]\n", a->name, a->data.i);
        }

        if(has_admin == false) {
            debug("error: no level 10 admins configured.\n");
            goto error;
        }

    //  load channels
        if(tree_descend(n, "channels", TREE_NODE_LIST, false, &a) == 0) {
            debug("  channels:\n");
            tree_node_for_each(a,a) {
                if(a->type == TREE_NODE_STR) {
                    ch_argv = strsplit(a->data.str, ":", &ch_argc);
                    if(ch_argv == NULL)
                        goto error;

                    e = irc_conn_add_channel(c, ch_argv[0], ch_argc > 1 ? ch_argv[1] : NULL, &ch);
                    strbuf_free(ch_argv, ch_argc);

                    if(e != 0)
                        goto error;

                    debug("    %s\n", a->data.str);
                }
            }
        }

        if(c->channels.used_size == 0) {
            debug("    error: no channels configured\n");
            goto error;
        }

    //  store connection
        e = map_insert(&bot->conns, c->name, c, free);
        if(e != 0)
            goto error;

        continue;
    }

    return 0;

error:
    irc_conn_destroy(c);
    return e;
}

inline static
int load_modules (struct irc_bot *bot, struct tree_node *n) {
    int e;
    char *name;
    struct irc_module *m;

    debug("loading modules ...\n");

    for(; n != NULL; n = n->next) {
        if(n->type != TREE_NODE_STR)
            continue;

        name = n->data.str;

        if(irc_module_compile(name) == 0 &&
            irc_module_new(bot, name, &m) == 0 &&
            irc_module_init(bot, m) == 0)
        {
            debug("  %s\n", name);
        }
    }

    return 0;
}

int irc_bot_init (struct irc_bot *bot) {
    int e;
    size_t i;
    struct irc_command_tpl *cmd;
    struct irc_listener_tpl *listener;

    pthread_mutex_init(&bot->lock, NULL);
    memset(&bot->tm_thr, 0, sizeof(bot->tm_thr));

    bot->active = false;

    bot->defaults.nick = NULL;
    bot->defaults.user = NULL;
    bot->defaults.real_name = NULL;
    bot->defaults.quit_msg = NULL;

    bot->cmd_prefix = NULL;
    bot->cmd_prefix_len = 0;
    bot->cmd_throttle = 3;

    e = list_init(&bot->listeners, 16, 8);
    if(e != 0)
        goto e1;

    e = list_init(&bot->timers, 8, 8);
    if(e != 0)
        goto e2;

    e = map_init(&bot->commands, 16, 8);
    if(e != 0)
        goto e3;

    e = map_init(&bot->conns, 8, 8);
    if(e != 0)
        goto e4;

    e = map_init(&bot->modules, 8, 8);
    if(e != 0)
        goto e5;

    e = irc_bot_set_cmd_prefix(bot, DEFAULT_CMD_PREFIX);
    if(e != 0)
        goto e6;

//  add core commands
    for(i = 0; i < lsdbot_core_commands_len; ++i) {
        cmd = &lsdbot_core_commands[i];
        e = irc_bot_add_command(bot, cmd->name, cmd->access, cmd->callback, cmd->min_argc, cmd->max_argc, cmd->max_split);
        if(e != 0)
            goto e6;
    }

//  add core listeners
    for(i = 0; i < lsdbot_core_listeners_len; ++i) {
        listener = &lsdbot_core_listeners[i];
        e = irc_bot_add_listener(bot, NULL, listener->action, listener->callback, false);
        if(e != 0)
            goto e6;
    }

    return 0;

e6: map_deinit(&bot->modules);
e5: map_deinit(&bot->conns);
e4: map_deinit(&bot->commands);
e3: list_deinit(&bot->timers);
e2: list_deinit(&bot->listeners);
e1: pthread_mutex_destroy(&bot->lock);

    return e;
}

int irc_bot_deinit (struct irc_bot *bot) {
    struct irc_command *cmd;

    if(bot->active)
        irc_bot_stop(bot);

    map_deinit(&bot->modules);
    map_deinit(&bot->conns);
    map_deinit(&bot->commands);

    list_deinit(&bot->timers);
    list_deinit(&bot->listeners);
    pthread_mutex_destroy(&bot->lock);

    return 0;
}

int irc_bot_save_config (struct irc_bot *bot, char *path) {
    int e;
    size_t i[2],
           j[2],
           len;
    struct tree_node_mapping *o;
    struct tree_node *r,   // root node
                     *dn1, // 1st level dict node
                     *dn2, // 2nd level dict node
                     *ln,  // list node
                     *vn;  // values
    struct irc_conn *c;
    char *conn_name,
         *user_name,
         ch_str[256],
         *module_name;
    int32_t *level;
    struct irc_user *u;
    struct irc_channel *ch;
    struct irc_module *m;
    FILE *fh;
    char *buf;

    r = tree_node_new(TREE_NODE_DICT, NULL, NULL);
    if(r == NULL) {
        e = -1;
        goto error;
    }

//  bot-specific options and default connection settings
    struct tree_node_mapping bot_opts[] = {
        { "cmd_prefix",         TREE_NODE_STR, bot->cmd_prefix         },
        { "cmd_throttle",       TREE_NODE_INT, &bot->cmd_throttle      },
        { "defaults.nick",      TREE_NODE_STR, bot->defaults.nick      },
        { "defaults.user",      TREE_NODE_STR, bot->defaults.user      },
        { "defaults.real_name", TREE_NODE_STR, bot->defaults.real_name },
        { "defaults.quit_msg",  TREE_NODE_STR, bot->defaults.quit_msg  }
    };

    e = tree_set_multi(r, bot_opts, sizeof(bot_opts) / sizeof(bot_opts[0]));
    if(e != 0)
        goto error;

    debug("> stored bot options\n");

//  root connections node
    dn1 = tree_node_new(TREE_NODE_DICT, "connections", r);
    if(dn1 == NULL) {
        e = -1;
        goto error;
    }

    map_for_each(&bot->conns, i[0], j[0], conn_name, c) {
        dn2 = tree_node_new(TREE_NODE_DICT, conn_name, dn1);
        if(dn2 == NULL) {
            e = -1;
            goto error;
        }

        struct tree_node_mapping conn_opts[] = {
            { "host",     TREE_NODE_STR, c->host },
            { "port",     TREE_NODE_INT, &c->port },
            { "password", TREE_NODE_STR, c->password }
        };

        e = tree_set_multi(dn2, conn_opts, sizeof(conn_opts) / sizeof(conn_opts[0]));
        if(e != 0) {
            e = -1;
            goto error;
        }

    //  nick
        if(strcmp(c->nick, c->bot->defaults.nick) != 0) {
            vn = tree_node_new(TREE_NODE_STR, "nick", dn2);
            if(vn == NULL) {
                e = -1;
                goto error;
            }
        }

    //  user
        if(strcmp(c->user, c->bot->defaults.user) != 0) {
            vn = tree_node_new(TREE_NODE_STR, "user", dn2);
            if(vn == NULL) {
                e = -1;
                goto error;
            }
        }

    //  real name
        if(c->real_name != NULL && strcmp(c->real_name, c->bot->defaults.real_name) != 0) {
            vn = tree_node_new(TREE_NODE_STR, "real_name", dn2);
            if(vn == NULL) {
                e = -1;
                goto error;
            }
        }

    //  quit message
        if(c->quit_msg != NULL && strcmp(c->quit_msg, c->bot->defaults.quit_msg) != 0) {
            vn = tree_node_new(TREE_NODE_STR, "quit_msg", dn2);
            if(vn == NULL) {
                e = -1;
                goto error;
            }
        }

    //  nickserv stuff
        e = tree_set(dn2, "nickserv", TREE_NODE_STR, c->nickserv);
        if(e != 0)
            goto error;

        e = tree_set(dn2, "nickserv_password", TREE_NODE_STR, c->nickserv_password);
        if(e != 0)
            goto error;

    //  acl
        ln = tree_node_new(TREE_NODE_DICT, "access", dn2);
        if(ln == NULL) {
            e = -1;
            goto error;
        }

        map_for_each(&c->access, i[1], j[1], user_name, level) {
            e = tree_set(ln, user_name, TREE_NODE_INT, level);
            if(e != 0)
                goto error;

            debug(">> stored acl rule: %s [%i]\n", user_name, *level);
        }

    //  channels
        ln = tree_node_new(TREE_NODE_LIST, "channels", dn2);
        if(ln == NULL) {
            e = -1;
            goto error;
        }

        list_for_each(&c->channels, i[0], ch) {
            if(ch->key != NULL)
                snprintf(ch_str, sizeof(ch_str), "%s:%s", ch->name, ch->key);
            else
                snprintf(ch_str, sizeof(ch_str), "%s", ch->name);


            e = tree_set(ln, "[-1]", TREE_NODE_STR, ch_str);
            if(e != 0)
                goto error;

            debug(">> stored channel: %s\n", ch_str);
        }

        debug("> stored connection: %s\n", conn_name);
    }

//  loaded modules
    ln = tree_node_new(TREE_NODE_LIST, "modules", r);
    if(ln == NULL) {
        e = -1;
        goto error;
    }

    map_for_each(&bot->modules, i[0], j[0], module_name, m) {
        e = tree_set(ln, "[-1]", TREE_NODE_STR, module_name);
        if(e != 0)
            goto error;

        debug(">> stored module: %s\n", module_name);
    }

    debug("> stored loaded modules\n");

//  write to file
    fh = fopen(path, "w+");
    if(fh == NULL) {
        e = -1;
        goto error;
    }

    tree_write(r, "    ", fh);
    tree_node_destroy(r);
    fclose(fh);

    return 0;

error:

    debug("error: cannot save configuration (%i)\n", e);
    tree_node_destroy(r);

    return e;
}

int irc_bot_load_config (struct irc_bot *bot, char *path) {
    int e;
    FILE *fh;
    struct tree_node *r,
                     *n,
                     *a;
    size_t nr;
    char *buf,
         *cmd_prefix,
         *default_nick,
         *default_user,
         *default_real_name,
         *default_quit_msg;
    time_t cmd_throttle = -1;

    struct tree_node_mapping bot_opts[] = {
        { "cmd_prefix",         TREE_NODE_STR, &cmd_prefix        },
        { "cmd_throttle",       TREE_NODE_INT, &cmd_throttle      },
        { "defaults.nick",      TREE_NODE_STR, &default_nick      },
        { "defaults.user",      TREE_NODE_STR, &default_user      },
        { "defaults.real_name", TREE_NODE_STR, &default_real_name },
        { "defaults.quit_msg",  TREE_NODE_STR, &default_quit_msg  }
    };

//  load and deserialize configuration
    fh = fopen(path, "r");
    if(fh == NULL) {
        debug("error: cannot open config\n");
        return 1;
    }

    e = freadall(&buf, &nr, 4096, fh);
    if(e == -1) {
        debug("error: out of memory\n");
        return 1;
    }
    else if(e == 1) {
        debug("error: cannot read config\n");
        return 1;
    }

    fclose(fh);

    e = tree_deserialize(&r, buf);
    if(e != 0) {
        debug("error: cannot parse config [%i]\n", e);
        return 1;
    }

//  load bot-specific options and connection defaults
    tree_get_multi(r, bot_opts, sizeof(bot_opts) / sizeof(bot_opts[0]));

    if(cmd_prefix != NULL)
        irc_bot_set_cmd_prefix(bot, cmd_prefix);
    if(cmd_throttle != -1)
        irc_bot_set_cmd_throttle(bot, cmd_throttle);

    irc_bot_set_defaults(bot, default_nick, default_user, default_real_name, default_quit_msg);

    debug("cmd_prefix: %s\n", bot->cmd_prefix);
    debug("default nick: %s\n", bot->defaults.nick);
    debug("default user: %s\n", bot->defaults.user);
    debug("default real name: %s\n", bot->defaults.real_name);
    debug("default quit msg: %s\n", bot->defaults.quit_msg);

//  load connections
    if(tree_descend(r, "connections", TREE_NODE_DICT, false, &n) == 0) {
        e = load_connections(bot, n->child);
        if(e != 0)
            goto cleanup;
    }
    else {
        e = 1;
        debug("error: no connections configured\n");
        goto cleanup;
    }

//  load modules
    if(tree_descend(r, "modules", TREE_NODE_LIST, false, &n) == 0) {
        e = load_modules(bot, n->child);
        if(e != 0)
            goto cleanup;
    }
    else {
        debug("notice: no modules configured\n");
    }

cleanup:

    tree_node_destroy(r);
    return e;
}

int irc_bot_set_cmd_prefix (struct irc_bot *bot, char *prefix) {
    prefix = strdup(prefix);
    if(prefix == NULL)
        return -1;

    bot->cmd_prefix = prefix;
    bot->cmd_prefix_len = strlen(prefix);

    return 0;
}

void irc_bot_set_cmd_throttle (struct irc_bot *bot, time_t throttle) {
    bot->cmd_throttle = throttle;
}

int irc_bot_set_defaults (struct irc_bot *bot, char *nick, char *user, char *real_name, char *quit_msg) {
    if(nick != NULL) {
        bot->defaults.nick = strdup(nick);
        if(bot->defaults.nick == NULL)
            return -1;
    }

    if(user != NULL) {
        bot->defaults.user = strdup(user);
        if(bot->defaults.user == NULL)
            return -1;
    }

    if(real_name != NULL) {
        bot->defaults.real_name = strdup(real_name);
        if(bot->defaults.real_name == NULL)
            return -1;
    }

    if(quit_msg != NULL) {
        bot->defaults.quit_msg = strdup(quit_msg);
        if(bot->defaults.quit_msg == NULL)
            return -1;
    }

    return 0;
}

int irc_bot_add_command (struct irc_bot *bot, char *tag, char *name, unsigned int access, irc_command_callback *callback, size_t min_argc, ssize_t max_argc, ssize_t max_split) {
    int e;
    struct irc_command *cmd;

    cmd = malloc(sizeof(struct irc_command));
    if(cmd == NULL)
        return -1;

    cmd->tag = tag;
    cmd->access = access;
    cmd->callback = callback;
    cmd->min_argc = min_argc;
    cmd->max_argc = max_argc;
    cmd->max_split = max_split;

    e = map_insert(&bot->commands, name, cmd, free);
    if(e != 0) {
        free(cmd);
        return e;
    }

    return 0;
}

int irc_bot_add_listener (struct irc_bot *bot, struct irc_conn *c, char *action, irc_listener_callback *callback, bool tmp) {
    struct irc_listener *l;
    int e;

    l = malloc(sizeof(struct irc_listener));
    if(l == NULL)
        return -1;

    l->conn = c;

    l->action = strdup(action);
    if(l->action == NULL) {
        e = -1;
        goto e1;
    }

    l->callback = callback;
    l->tmp = tmp;

    e = list_append(&bot->listeners, l, listener_destroy);
    if(e != 0)
        goto e2;

    return 0;

e2: free(l->action);
e1: free(l);

    return e;
}

int irc_bot_rm_listener (struct irc_bot *bot, struct irc_conn *c, char *action, irc_listener_callback *callback) {
    size_t i;
    struct irc_listener *l;

    list_for_each(&bot->listeners, i,l) {
        if(l->conn == c &&
            strcmp(l->action, action) == 0 &&
            l->callback == callback)
        {
            list_remove(&bot->listeners, i);
            return 0;
        }
    }

    return 1;
}

int irc_bot_add_conn (struct irc_conn **c_ret, struct irc_bot *bot, char *name, char *host, int port, char *password, char *nick, char *user, char *real_name, char *quit_msg, char *nickserv, char *nickserv_password) {
    int e;
    struct irc_conn *c;

    if(name == NULL)
        return 1;

    e = irc_conn_new(&c, name, bot);
    if(c == NULL)
        return 1;

    if(c_ret != NULL)
        *c_ret = c;

    e = irc_conn_configure(c, host, port, password, nick, user, real_name, quit_msg, nickserv, nickserv_password);
    if(e != 0)
        goto e1;

    e = map_insert(&bot->conns, name, c, irc_conn_destroy);
    if(e != 0)
        goto e2;

    return 0;

e2: irc_conn_deinit(c);
e1: free(c);

    return e;
}

int irc_bot_rm_conn (struct irc_bot *bot, char *name) {
    int e;
    struct irc_conn *c;

    c = irc_bot_get_conn(bot, name);
    if(c == NULL)
        return 1;

    return map_remove(&bot->conns, name);
}

int irc_bot_start (struct irc_bot *bot) {
    int e;
    size_t i,j;
    char *k;
    struct irc_conn *c;

    bot->active = true;

//  start timer thread
    if(pthread_create(&bot->tm_thr, NULL, &timer_thread, bot) != 0)
        goto e1;

//  start connection threads
    map_for_each(&bot->conns, i,j,k,c) {
        if(pthread_create(&c->thr, NULL, &conn_thread, c) != 0)
            goto e2;
    }

    return 0;

e2: map_for_each(&bot->conns, i,j,k,c) {
        pthread_cancel(c->thr);
    }

    pthread_cancel(bot->tm_thr);
e1: bot->active = false;
    return -1;
}

void irc_bot_wait (struct irc_bot *bot) {
    size_t i,j;
    char *k;
    struct irc_conn *c;
    void *rval;

    while(bot->active)
        sleep(1);

    pthread_join(bot->tm_thr, &rval);

    map_for_each(&bot->conns, i,j,k,c) {
        if(c->fd != -1)
            pthread_join(c->thr, &rval);
    }
}

void irc_bot_stop (struct irc_bot *bot) {
    size_t i,j;
    char *k;
    struct irc_timer *t;
    struct irc_conn *c;

    bot->active = false;

    pthread_cancel(bot->tm_thr);

    list_for_each(&bot->timers, i,t) {
        irc_timer_destroy(t);
    }

    map_for_each(&bot->conns, i,j,k,c) {
        irc_quit(c, c->quit_msg);
        irc_conn_close(c);
        pthread_join(c->thr, NULL);
    }

    irc_bot_save_config(bot, "config.json");
}
