#include "lsdbot.h"

#include <clip/string.h>
#include <clip/term_colours.h>

#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>

// core.c
extern lsdbot_listener(on_001);

void channel_destroy (struct irc_channel *ch) {
    free(ch->name);
    free(ch->key);
    free(ch);
}

int irc_conn_new (struct irc_conn **c, char *name, struct irc_bot *bot) {
    int e;

    *c = malloc(sizeof(struct irc_conn));
    if(*c == NULL)
        return -1;

    e = irc_conn_init(*c, name, bot);
    if(e != 0)
        free(*c);

    return e;
}

void irc_conn_destroy (struct irc_conn *c) {
    irc_conn_deinit(c);
    free(c);
}

int irc_conn_init (struct irc_conn *c, char *name, struct irc_bot *bot) {
    int e;

    memset(&c->thr, 0, sizeof(pthread_t));
    c->bot = bot;

    c->fd = -1;
    c->port = -1;

    c->name = strdup(name);
    if(c->name == NULL)
        return -1;

    c->host = NULL;
    c->password = NULL;
    c->nick = NULL;
    c->user = NULL;
    c->real_name = NULL;
    c->quit_msg = NULL;

    memset(c->rbuf, 0, sizeof(c->rbuf));
    memset(c->wbuf, 0, sizeof(c->wbuf));

    e = map_init(&c->access, 8, 8);
    if(e != 0)
        goto e1;

    e = list_init(&c->users, 64, 8);
    if(e != 0)
        goto e2;

    e = list_init(&c->channels, 16, 8);
    if(e != 0)
        goto e3;

    return 0;

e3: list_deinit(&c->users);
e2: map_deinit(&c->access);
e1: free(c->name);

    return e;
}

void irc_conn_deinit (struct irc_conn *c) {
    irc_conn_close(c);

    free(c->name);
    free(c->host);
    free(c->password);
    free(c->nick);
    free(c->user);

    c->bot = NULL;
    c->name = NULL;
    c->host = NULL;
    c->password = NULL;
    c->nick = NULL;
    c->user = NULL;
    c->real_name = NULL;
    c->quit_msg = NULL;

    map_deinit(&c->access);
    list_deinit(&c->users);
    list_deinit(&c->channels);
}

int irc_conn_acl_add (struct irc_conn *c, char *user, unsigned int level) {
    int e,
       *levelp;

    user = strdup(user);
    if(user == NULL)
        return -1;

    levelp = malloc(sizeof(int));
    if(levelp == NULL) {
        free(user);
        return -1;
    }

    *levelp = level;

    e = map_insert(&c->access, user, levelp, free);
    if(e != 0) {
        free(user);
        free(levelp);
    }

    return e;
}

int irc_conn_add_user (struct irc_conn *c, char *name, char *nick, char *host, struct irc_user **u_ret) {
    int e;
    struct irc_user *u;

    u = irc_user_new(name, nick, NULL);
    if(u == NULL)
        return -1;

    e = list_append(&c->users, u, irc_user_destroy);
    if(e != 0) {
        irc_user_destroy(u);
        return e;
    }

    if(u_ret != NULL)
        *u_ret = u;

    return e;
}

int irc_conn_rm_user (struct irc_conn *c, char *name) {
    int e;
    size_t i;
    struct irc_user *u;

    list_for_each(&c->users, i,u) {
        if(strcmp(u->nick, name) == 0)
            return list_remove(&c->users, i);
    }

    list_for_each(&c->users, i,u) {
        if(u->name != NULL && strcmp(u->name, name) == 0)
            return list_remove(&c->users, i);
    }

    return 1;
}

struct irc_user *
irc_conn_get_user (struct irc_conn *c, char *name) {
    size_t i;
    struct irc_user *u,
                    *x;

    u = NULL;
    list_for_each(&c->users, i,x) {
        if(strcmp(x->nick, name) == 0) {
            u = x;
            break;
        }
    }

    if(u == NULL) {
        list_for_each(&c->users, i,x) {
            if(x->name != NULL && strcmp(x->name, name) == 0) {
                u = x;
                break;
            }
        }
    }

    return u;
}

int irc_conn_add_channel (struct irc_conn *c, char *name, char *key, struct irc_channel **ch_ret) {
    int e;
    struct irc_channel *ch;

    ch = malloc(sizeof(struct irc_channel));
    if(ch == NULL)
        return -1;

    ch->name = strdup(name);
    if(ch->name == NULL) {
        free(ch);
        return -1;
    }

    if(key != NULL) {
        ch->key = strdup(key);
        if(ch->key == NULL) {
            free(ch->name);
            free(ch);
            return -1;
        }
    }
    else {
        ch->key = NULL;
    }

    e = list_append(&c->channels, ch, channel_destroy);
    if(e != 0) {
        channel_destroy(ch);
        return e;
    }

    if(ch_ret != NULL)
        *ch_ret = ch;

    return 0;
}

int irc_conn_rm_channel (struct irc_conn *c, char *name) {
    size_t i;
    struct irc_channel *ch;

    list_for_each(&c->channels, i,ch) {
        if(strcmp(ch->name, name) == 0)
            return list_remove(&c->channels, i);
    }

    return 0;
}

struct irc_channel *
irc_conn_get_channel (struct irc_conn *c, char *name) {
    size_t i;
    struct irc_channel *ch;

    list_for_each(&c->channels, i,ch) {
        if(strcmp(ch->name, name) == 0)
            return ch;
    }

    return NULL;
}

int irc_conn_configure (struct irc_conn *c, char *host, int port, char *password, char *nick, char *user, char *real_name, char *quit_msg, char *nickserv, char *nickserv_password) {
    int e;
    bool reset;

    reset = c->fd != -1 && (
        port != c->port ||
        (host != NULL && c->host != NULL && strcmp(c->host, host) != 0) ||
        (user != NULL && c->user != NULL && strcmp(c->user, user) != 0)
    );

    c->port = port;

    if(host != NULL) {
        host = strdup(host);
        if(host == NULL)
            return -1;

        c->host = host;
    }
    else {
        c->host = NULL;
    }

    if(password != NULL) {
        password = strdup(password);
        if(password == NULL)
            return -1;

        c->password = password;
    }
    else {
        c->password = NULL;
    }

    if(nick != NULL) {
        nick = strdup(nick);
        if(nick == NULL)
            return -1;

        c->nick = nick;
    }
    else {
        c->nick = NULL;
    }

    if(user != NULL) {
        user = strdup(user);
        if(user == NULL)
            return -1;

        c->user = user;
    }
    else {
        c->user = NULL;
    }

    if(real_name != NULL) {
        real_name = strdup(real_name);
        if(real_name == NULL)
            return -1;

        c->real_name = real_name;
    }
    else {
        c->real_name = NULL;
    }

    if(quit_msg != NULL) {
        quit_msg = strdup(quit_msg);
        if(quit_msg == NULL)
            return -1;

        c->quit_msg = quit_msg;
    }
    else {
        c->quit_msg = NULL;
    }

    if(nickserv != NULL) {
        nickserv = strdup(nickserv);
        if(nickserv == NULL)
            return -1;

        c->nickserv = nickserv;
    }
    else {
        c->nickserv = NULL;
    }

    if(nickserv_password != NULL) {
        nickserv_password = strdup(nickserv_password);
        if(nickserv_password == NULL)
            return -1;

        c->nickserv_password = nickserv_password;
    }
    else {
        c->nickserv_password = NULL;
    }

    if(reset) {
        e = irc_quit(c, "reconfiguring");
        if(e != 0)
            return e;

        e = irc_conn_reset(c);
        if(e != 0)
            return e;
    }

    return 0;
}

int irc_conn_open (struct irc_conn *c) {
    int e,
        fd;
    char service[NI_MAXSERV];
    struct addrinfo hints,
                    *addrinfo,
                    *ai;
    struct timeval tv;

    if(c->fd != -1)
        return 0;

    if(c->port < 0)
        snprintf(service, NI_MAXSERV, "6667");
    else
        snprintf(service, NI_MAXSERV, "%i", c->port);

    hints = (struct addrinfo) {
        .ai_flags = 0,
        .ai_family = AF_UNSPEC,
        .ai_socktype = SOCK_STREAM,
        .ai_protocol = IPPROTO_TCP,
        .ai_addrlen = 0,
        .ai_addr = NULL,
        .ai_canonname = NULL,
        .ai_next = NULL
    };

    e = getaddrinfo(c->host, service, &hints, &addrinfo);
    if(e != 0)
        return EGETADDRINFO;

    for(ai = addrinfo; ai != NULL; ai = ai->ai_next) {
        fd = socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol);
        if(fd == -1) {
            e = ESOCKET;
            continue;
        }

        if(connect(fd, ai->ai_addr, ai->ai_addrlen) == -1) {
            close(fd);
            e = ECONNECT;
            continue;
        }

        c->fd = fd;
        e = 0;

        break;
    }

    freeaddrinfo(addrinfo);
    if(e != 0)
        return e;

    tv.tv_sec = 10;
    tv.tv_sec = 0;

    setsockopt(c->fd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(struct timeval));
    setsockopt(c->fd, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(struct timeval));

    if(c->password != NULL) {
        e = irc_password(c, c->password);
        if(e != 0)
            return e;
    }

    e = irc_user(c, c->user, true);
    if(e != 0)
        return e;

    e = irc_nick(c, c->nick);
    if(e != 0)
        return e;

    irc_bot_add_listener(c->bot, c, "001", on_001, true);

    return 0;
}

int irc_conn_close (struct irc_conn *c) {
    if(c->fd == -1)
        return 1;

    close(c->fd);
    c->fd = -1;

    return 0;
}

int irc_conn_reset (struct irc_conn *c) {
    int e;

    e = irc_conn_close(c);
    if(e != 0)
        return e;

    return irc_conn_open(c);
}

int irc_conn_read_line (struct irc_conn *c) {
    ssize_t nr;
    char *ptr;
    size_t len;

    ptr = c->rbuf;
    len = 0;

    while(true) {
        nr = recv(c->fd, ptr, 1, MSG_NOSIGNAL);
        if(nr <= 0)
            return nr;

        ++ptr;
        ++len;

        if(len > 1 && strncmp(ptr - 2, "\r\n", 2) == 0) {
            *ptr = '\0';
            break;
        }

        if(len == sizeof(c->rbuf)) {
            return 1;
        }
    }

    debug(TERM_FG(0,GRN) "< %s" TERM_RESET, c->rbuf);

    return 0;
}

int irc_conn_send_line (struct irc_conn *c) {
    size_t len;
    ssize_t ns;

    ns = send(c->fd, c->wbuf, strlen(c->wbuf), MSG_NOSIGNAL|MSG_WAITALL);
    if(ns == -1)
        return -1;

    debug(TERM_FG(0,YLW) "> %s" TERM_RESET, c->wbuf);
    memset(c->wbuf, 0, sizeof(c->wbuf));

    return 0;
}

int irc_conn_iter (struct irc_conn *c, struct irc_event *ev) {
    int e;

    e = irc_conn_read_line(c);
    if(e != 0)
        return e;

    e = irc_parse_line(ev, c, c->rbuf);
    if(e != 0)
        return e;

    return irc_event_process(ev);
}
