{
    "cmd_prefix": ";",
    "defaults": {
        "nick": "insanitybot",
        "user": "lsdbot",
        "real_name": "lsdbot",
        "quit_msg": "wat"
    },
    "connections": {
        "something": {
            "host": null,
            "port": 6667,
            "password": null,
            "nickserv": "NickServ",
            "nickserv_password": NULL,
            "access": {
                "someone": 10
            },
            "channels": [
                "##lsdbot"
            ]
        }
    },
    "modules": [
        "choice",
        "wat",
        "quiet",
        "banwords"
    ]
}
