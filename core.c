#include "lsdbot.h"

#include <clip/map.h>
#include <clip/string.h>
#include <ctype.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

// predeclarations

lsdbot_command(cmd_whois);

lsdbot_command(cmd_say);
lsdbot_command(cmd_action);
lsdbot_command(cmd_notice);

lsdbot_command(cmd_nick);
lsdbot_command(cmd_join);
lsdbot_command(cmd_part);
lsdbot_command(cmd_kick);
lsdbot_command(cmd_mode);
lsdbot_command(cmd_names);
lsdbot_command(cmd_quit);
lsdbot_command(cmd_identify);
lsdbot_command(cmd_config);
lsdbot_command(cmd_access);
lsdbot_command(cmd_reload);
lsdbot_command(cmd_exit);

lsdbot_listener(on_ping);
lsdbot_listener(on_privmsg);
lsdbot_listener(on_join);
lsdbot_listener(on_leave);
lsdbot_listener(on_quit);
lsdbot_listener(on_nick);
lsdbot_listener(on_mode);
lsdbot_listener(on_319);
lsdbot_listener(on_330);
lsdbot_listener(on_353);

// ----

struct irc_command_tpl
lsdbot_core_commands[] = {
    { "whois",       0, cmd_whois,      0, -1, -1 },

    { "say",         9, cmd_say,        2,  2,  1 },
    { "action",      9, cmd_action,     2,  2,  1 },
    { "notice",      9, cmd_notice,     2,  2,  1 },

    { "nick",       10, cmd_nick,       1,  1, -1 },
    { "join",       10, cmd_join,       1, -1, -1 },
    { "part",       10, cmd_part,       1,  2,  1 },
    { "kick",       10, cmd_kick,       2,  3,  2 },
    { "mode",       10, cmd_mode,       2,  3, -1 },
    { "quit",       10, cmd_quit,       0,  0, -1 },
    { "names",      10, cmd_names,      0,  1, -1 },
    { "identify",   10, cmd_identify,   1,  1,  0 },
    { "config",     10, cmd_config,     1,  1, -1 },
    { "access",     10, cmd_access,     1, -1, -1 },
    { "reload",     10, cmd_reload,     0, -1,  0 },
    { "exit",       10, cmd_exit,       0, -1,  0 },
};

size_t lsdbot_core_commands_len =
    sizeof(lsdbot_core_commands) / sizeof(lsdbot_core_commands[0]);

struct irc_listener_tpl
lsdbot_core_listeners[] = {
    { "PING",    on_ping    },
    { "PRIVMSG", on_privmsg },
    { "JOIN",    on_join    },
    { "PART",    on_leave   },
    { "QUIT",    on_quit    },
    { "KICK",    on_leave   },
    { "NICK",    on_nick    },
    { "MODE",    on_mode    },
    { "319",     on_319     },
    { "330",     on_330     },
    { "353",     on_353     },
};

size_t lsdbot_core_listeners_len =
    sizeof(lsdbot_core_listeners) / sizeof(lsdbot_core_listeners[0]);

// core commands

lsdbot_command(cmd_whois) {
    size_t i;

    if(argc > 0) {
        for(i = 0; i < argc; ++i)
            irc_whois(ev->conn, argv[i]);

        return 0;
    }

    return irc_whois(ev->conn, ev->nick);
}

lsdbot_command(cmd_say) {
    return irc_privmsg(ev->conn, argv[0], argv[1]);
}

lsdbot_command(cmd_action) {
    return irc_action(ev->conn, argv[0], argv[1]);
}

lsdbot_command(cmd_notice) {
    return irc_notice(ev->conn, argv[0], argv[1]);
}

lsdbot_command(cmd_nick) {
    return irc_nick(ev->conn, argv[0]);
}

lsdbot_command(cmd_join) {
    int e;
    size_t n,i;
    char **ch;
    size_t c;
    char *ch_name,
         *ch_key,
         *ch_names;

    n = 0;
    for(i = 0; i < argc; ++i) {
        ch = strsplit(argv[i], ":", &c);
        if(ch == NULL)
            continue;

        ch_name = ch[0];
        ch_key = c > 1 ? ch[1] : NULL;

        if(irc_join(ev->conn, ch_name, ch_key) == 0) {
            irc_conn_add_channel(ev->conn, ch_name, ch_key, NULL);
            ++n;
        }

        strbuf_free(ch, c);
    }

    ch_names = strbuf_join(argv, ", ", argc);
    irc_reply(ev, "Joined %zu %s: %s", n, n == 1 ? "channel" : "channels", ch_names);

    return 0;
}

lsdbot_command(cmd_part) {
    int e;
    char *part_msg;

    part_msg = argc > 1 ? argv[1] : ev->conn->part_msg;
    e = irc_part(ev->conn, argv[0], part_msg);


    if(e == 0)
        irc_reply(ev, "Parted %s.", argv[0]);
    else
        irc_reply(ev, "Invalid channel.");

    return 0;
}

lsdbot_command(cmd_kick) {
    return irc_kick(ev->conn, argv[0], argv[1], argc > 2 ? argv[2] : NULL);
}

lsdbot_command(cmd_mode) {
    return irc_mode(ev->conn, argv[0], argv[1], argc > 2 ? argv[2] : NULL);
}

lsdbot_command(cmd_names) {
    int e;
    if(argc == 1)
        e = irc_names(ev->conn, argv[0]);
    else
        e = irc_names(ev->conn, NULL);

    return e;
}

lsdbot_command(cmd_quit) {
    int e;
    void *rval;

    e = irc_quit(ev->conn, argc > 0 ? argv[0] : ev->conn->quit_msg);
    if(e != 0)
        return e;

    pthread_join(ev->conn->thr, &rval);
    return 0;
}

lsdbot_command(cmd_identify) {
    if(ev->conn->nickserv != NULL)
        return irc_identify(ev->conn, argv[0]);
    else
        return 1;
}

lsdbot_command(cmd_config) {
    return 0;
}

lsdbot_command(cmd_access) {
    int e;
    struct irc_user *u;

    u = irc_conn_get_user(ev->conn, argv[0]);
    if(u == NULL) {
        irc_reply(ev, "No such user.");
        return 1;
    }

    u->access = strtol(argv[1], NULL, 10);

    irc_whois(ev->conn, argv[0]);
    irc_reply(ev, "%s now has level %u access.", argv[0], u->access);

    return 0;
}

lsdbot_command(cmd_reload) {
    size_t n,i,j;
    char *name;
    struct irc_module *m;

    n = 0;

    if(argc == 1 && strcmp(argv[0], "-core") == 0) {
        int compile_pid,
            status;
        char *name,
             tmp_bin_arg[256],
             *bin_arg,
             **bin_argv;
        struct irc_conn *conn;

        char *compile_argv[2] = { "./setup", NULL };

        compile_pid = fork();
        if(compile_pid == 0)
            execv("build", compile_argv);

    //  wait for compilation to finish
        waitpid(compile_pid, &status, 0);

        bin_argv = malloc(sizeof(char *) * (2 + map_item_count(&ev->conn->bot->conns)));
        if(bin_argv == NULL)
            return -1;

        bin_argv[n++] = "./lsdbot";
        map_for_each(&ev->conn->bot->conns, i,j, name,conn) {
            snprintf(tmp_bin_arg, sizeof(tmp_bin_arg), "%s:%i", conn->name, conn->fd);
            bin_arg = strdup(tmp_bin_arg);
            if(bin_arg == NULL)
                goto cleanup;

            bin_argv[n++] = bin_arg;
        }

        bin_argv[n] = NULL;
        irc_reply(ev, "%s", "Core recompiled, restarting..");
        execv("lsdbot", bin_argv);

        return -1;

    cleanup:
        if(i > 0) for(++i; i > 0; --i)
            free(bin_argv[i]);

        free(bin_argv);
        return -1;
    }
    else if(argc > 0) {
        for(i = 0; i < argc; ++i) {
            name = argv[i];
            m = map_get(&ev->conn->bot->modules, name);
            if(m != NULL &&
                irc_module_compile(name) == 0 &&
                irc_module_deinit(ev->conn->bot, m) == 0 &&
                irc_module_init(ev->conn->bot, m) == 0)
            {
                ++n;
            }
        }
    }
    else {
        map_for_each(&ev->conn->bot->modules, i,j, name, m) {
            if(irc_module_compile(name) == 0 &&
                irc_module_deinit(ev->conn->bot, m) == 0 &&
                irc_module_init(ev->conn->bot, m) == 0)
            {
                ++n;
            }
        }
    }

    irc_reply(ev, "%zi %s reloaded.", n, n == 1 ? "module" : "modules" );
    return 0;
}

lsdbot_command(cmd_exit) {
    irc_bot_stop(ev->conn->bot);
    return 0;
}

// core listeners

// 001 [self] :<msg>
// Temporary listener for initializing core and joining channels.

lsdbot_listener(on_001) {
    int e;
    size_t len,
           i,j,y,z;
    struct irc_channel *ch;

    if(ev->conn->nickserv_password != NULL)
        irc_identify(ev->conn, ev->conn->nickserv_password);

//  join channels
    list_for_each(&ev->conn->channels, i,ch) {
        irc_join(ev->conn, ch->name, ch->key);
    }

    return 0;
}

lsdbot_listener(on_ping) {
    irc_pong(ev->conn, ev->message);
    return 0;
}

lsdbot_listener(on_privmsg) {
    int e;
    time_t throttle_diff;
    char *ptr,
         *sp,
          sc,
         *cmd_name;
    struct irc_event rsp_ev;
    bool b;
    char **argv;
    size_t argc;
    struct irc_command *cmd;

    if(ev->user == NULL)
        return 0;

    sp = NULL;
    sc = 0;

//  check if message is a command
    if(ev->message != NULL && strncmp(ev->message, ev->conn->bot->cmd_prefix, ev->conn->bot->cmd_prefix_len) == 0) {
        throttle_diff = ev->time - ev->user->last_cmd;
        if(throttle_diff <= ev->conn->bot->cmd_throttle) {
            irc_reply(ev, "Try again in a few seconds.");
            return 1;
        }

        ptr = ev->message + ev->conn->bot->cmd_prefix_len;

    //  tokenize string
        cmd_name = strtoken(ptr, " ", &ptr, &sp, &sc);
        if(cmd_name == NULL) {
            cmd_name = ptr; // single argument
            ptr = NULL;
        }

    //  copy command name, restore
        cmd_name = strdup(cmd_name);
        strtoken(NULL, NULL, NULL, &sp, &sc);
        if(cmd_name == NULL)
            return -1;

    //  get command struct
        cmd = irc_bot_get_command(ev->conn->bot, cmd_name);
        if(cmd == NULL) {
            e = 1;
            goto cmd_fail;
        }

    //  check if command needs special access
        if(cmd->access > 0) {
        //  automatic user discovery
        //  -1 means a WHOIS hasn't been on the user done yet
            if(ev->user->access == -1 && irc_discover(ev->conn, ev->user->nick) != 0)
                goto cmd_fail;

        //  fail if authorization isn't valid
            if(irc_user_auth(ev->user, cmd->access) == false)
                goto cmd_fail;
        }

    //  split arguments, if any
        if(ptr != NULL) {
            while(isspace(*ptr))
                ++sp;

            argv = strnsplitq(ptr, cmd->max_split, &argc);
            if(argv == NULL) {
                e = -1;
                goto cmd_fail;
            }
        }
        else {
            argv = NULL;
            argc = 0;
        }

    //  check if command constraints are met, call it if so
        if(argc >= cmd->min_argc && (cmd->max_argc < 0 || argc <= cmd->max_argc))
            cmd->callback(ev, argv, argc);
        else
            irc_reply(ev, "Syntax for %s: %s", cmd_name, cmd->syntax ? cmd->syntax : "N/A");

        strbuf_free(argv, argc);
        free(cmd_name);
    }

    return 0;

cmd_fail:
    free(cmd_name);
    return 1;
}

lsdbot_listener(on_join) {
    int e;

    if(ev->user == NULL) {
        e = irc_conn_add_user(ev->conn, NULL, ev->nick, ev->host, &ev->user);
        if(e != 0)
            return e;
    }

    e = irc_user_update_flags(ev->user, ev->channel, false, false);
    if(e != 0)
        return e;

    debug("* JOIN [%s, %s]\n", ev->nick, ev->channel);
    return 0;
}

lsdbot_listener(on_leave) {
    int e;
    struct irc_user *u;

    if(strcmp(ev->action, "KICK") == 0)
        u = ev->target_user;
    else
        u = ev->user;

    if(u == NULL)
        return 1;

    if(strcmp(u->nick, ev->conn->nick) == 0)
        irc_conn_rm_channel(ev->conn, ev->channel);

    e = map_remove(&u->flags, ev->channel);
    if(e != 0)
        return e;

    debug("* %s [%s, %s]\n", ev->action, u->nick, ev->channel);
    return 0;
}

lsdbot_listener(on_nick) {
    int e;
    char *old_nick,
         *new_nick,
         *tmp;

    old_nick = ev->nick;
    new_nick = ev->message;

    if(strcmp(old_nick, ev->conn->nick) == 0) {
        free(ev->conn->nick);

        tmp = strdup(new_nick);
        if(tmp == NULL)
            return -1;

        ev->conn->nick = tmp;
    }

    if(ev->user != NULL) {
        e = irc_user_update(ev->user, NULL, new_nick, NULL);
        if(e != 0)
            return e;
    }

    debug("* NICK [%s -> %s]\n", old_nick, new_nick);
    return 0;
}

lsdbot_listener(on_mode) {
    size_t ai;
    uint8_t changes,
            flags,
            mask;
    char *ptr,
         *flagstr;
    struct irc_user *u;

    if(ev->argc < 4)
        return 0;

    ai = 2;
    changes = 0x00;
    flags = 0x00;
    mask = 0xFF;

    for(ptr = ev->argv[2]; *ptr != '\0'; ++ptr) {
        if(isalpha(*ptr) == false) {
            if(ai < ev->argc) {
                u = irc_conn_get_user(ev->conn, ev->argv[ai++]);
                if(u != NULL) {
                    irc_user_update_flags(u, ev->channel, changes, flags);
                    debug("*  MODE [%s %c%c%c]\n", u->nick,
                          (flags & FLAG_OP)    ? 'o':'-',
                          (flags & FLAG_VOICE) ? 'v':'-',
                          (flags & FLAG_QUIET) ? 'q':'-');
                }
            }

            changes = 0;
            flags = 0;

            if(*ptr == '+')
                mask = 0xFF;
            else
                mask = 0x00;
        }
        else if(*ptr == 'o') {
            changes |= FLAG_OP;
            flags |= FLAG_OP & mask;
        }
        else if(*ptr == 'v') {
            changes |= FLAG_VOICE;
            flags |= FLAG_VOICE & mask;
        }
        else if(*ptr == 'q') {
            changes |= FLAG_QUIET;
            flags |= FLAG_VOICE & mask;
        }
    }

    return 0;
}

lsdbot_listener(on_quit) {
    if(ev->user != NULL && ev->user->access <= 0)
        irc_conn_rm_user(ev->conn, ev->nick);

    debug("* QUIT [%s]\n", ev->nick);
    return 0;
}

// WHOIS
// 319 [self] <nick> :<channels>
// Updates flags for a single user in associated channels.

lsdbot_listener(on_319) {
    char *nick;
    struct irc_user *u;
    char **channels;
    size_t channelc,
           i;
    int e;
    char *ch_name;
    bool op,
         voice;

    nick = ev->argv[2];

    u = irc_conn_get_user(ev->conn, nick);
    if(u == NULL)
        return 1;

    channels = strsplit(ev->message, " ", &channelc);
    if(channels == NULL)
        return -1;

    e = 0;

    for(i = 0; i < channelc; ++i) {
        irc_parse_flags(channels[i], &ch_name, &op, &voice);
        e = irc_user_update_flags(u, ch_name, op, voice);
        if(e != 0)
            break;
    }

    strbuf_free(channels, channelc);

    debug("* 319 WHOIS [%s, %zu %s]\n", nick, channelc, channelc != 1 ? "channels" : "channel");
    return e;
}

// WHOIS
// 330 [self] <nick> <name> :is logged in as
// Updates the 'name' (primary nick) for a user.

lsdbot_listener(on_330) {
    char *nick,
         *name;
    struct irc_user *u;
    int e;
    size_t i,j;
    char *xname;
    unsigned int *level;

    nick = ev->argv[2];
    name = ev->argv[3];

    u = irc_conn_get_user(ev->conn, nick);
    if(u == NULL)
        return 1;

    e = irc_user_update(u, name, nick, NULL);
    if(e != 0)
        return e;

//  if user has access privileges, add/update acl entry
    if(u->access > 0) {
        level = map_get(&ev->conn->access, name);
        if(level != NULL)
            *level = u->access;
        else
            irc_conn_acl_add(ev->conn, name, u->access);
    }
//  otherwise check if one exists and if so, apply it
    else {
        u->access = 0; // -1 means no whois has been done yet
        map_for_each(&ev->conn->access, i,j, xname, level) {
            if(strcmp(xname, u->name) == 0) {
                u->access = *level;
                break;
            }
        }
    }

    debug("* 330 WHOIS [%s -> %s]\n", nick, name);
    return 0;
}

// NAMES
// 353 [self] = <channel> :<nicks>
// Updates flags for all users in a single channel.

lsdbot_listener(on_353) {
    char *ch_name;
    char **nicks;
    size_t nickc,
           i;
    int e;
    char *nick;
    bool op,
         voice;
    struct irc_user *u;

    ch_name = ev->argv[3];

    nicks = strsplit(ev->message, " ", &nickc);
    if(nicks == NULL)
        return -1;

    e=0;
    for(i=0; i < nickc; ++i) {
        irc_parse_flags(nicks[i], &nick, &op, &voice);

        u = irc_conn_get_user(ev->conn, nick);
        if(u == NULL) {
            e = irc_conn_add_user(ev->conn, NULL, nick, NULL, &u);
            if(e != 0)
                continue;
        }

        irc_user_update_flags(u, ch_name, op, voice);
    }

    strbuf_free(nicks, nickc);

    debug("* 353 NAMES [%s]\n", ch_name);
    return e;
}
