#include <ctype.h>
#include <lsdbot.h>
#include <string.h>

struct list quiets;

struct quiet_ctx {
    struct irc_conn *conn;
    struct irc_user *user;
    char *channel;
};

void quiet_ctx_free (struct quiet_ctx *q) {
    free(q->channel);
    free(q);
}

lsdbot_timer(do_unquiet) {
    struct quiet_ctx *q;

    q = data;
    irc_mode(q->conn, q->channel, "-q", q->user->nick);
    list_remove_data(&quiets, q);

    return 1;
}

lsdbot_listener(on_mode) {
    size_t i;
    struct quiet_ctx *q;

//  ignore if self-triggered or no user arg
    if(strcmp(ev->nick, ev->conn->nick) == 0 || ev->argc == 2)
        return 0;

    list_for_each(&quiets, i,q) {
        if(q->user != NULL &&
           strcmp(q->channel, ev->channel) == 0 &&
           strcmp(q->user->nick, ev->argv[2]) == 0)
        {
            list_remove(&quiets, i--);
        }
    }

    return 0;
}

lsdbot_command(quiet) {
    int e;
    size_t i;
    struct quiet_ctx *q;
    char *name,
         *duration,
         *msg;
    struct irc_user *u;
    uint8_t *f;
    struct irc_timer *t;

    if(ev->channel == NULL)
        return 1;

    list_for_each(&quiets, i,q) {
        if(strcmp(q->user->nick, ev->argv[2]) == 0)
            return 1;
    }

    name = argv[0];
    duration = argv[1];
    msg = argv[2];

    u = irc_conn_get_user(ev->conn, name);
    if(u == NULL) {
        irc_reply(ev, "Who is that?");
        return 1;
    }

    f = irc_user_get_flags(u, ev->channel);
    if(*f & FLAG_QUIET) {
        irc_reply(ev, "User is already quieted.");
        return 1;
    }

    q = malloc(sizeof(struct quiet_ctx));
    if(q == NULL)
        return -1;

    q->conn = ev->conn;
    q->user = u;

    q->channel = strdup(ev->channel);
    if(q->channel == NULL) {
        free(q);
        return -1;
    }

    irc_mode(q->conn, q->channel, "+q", u->nick);

    if(isdigit(duration[0])) {
        t = irc_timer_new(ev->conn, duration, do_unquiet, q);
        if(t == NULL) {
            quiet_ctx_free(q);
            return -1;
        }

        irc_timer_start(t);
        irc_reply(ev, "You have been quieted for %s; %s", duration, msg);
    }
    else {
        irc_reply(ev, "You have been quieted; %s", msg);
    }

    e = list_append(&quiets, q, quiet_ctx_free);
    if(e != 0) {
        free(q);
        irc_timer_destroy(t);
        return e;
    }

    return 0;
}

lsdbot_command(unquiet) {
    char *nick;
    struct irc_user *u;
    uint8_t *f;
    size_t i;
    struct quiet_ctx *q;
    char mask[128];

    nick = argv[0];

    u = irc_conn_get_user(ev->conn, nick);
    if(u == NULL) {
        irc_reply(ev, "%s: no such user.", nick, false);
        return 1;
    }

    f = irc_user_get_flags(u, ev->channel);
    if((*f & FLAG_QUIET) == 0) {
        irc_reply(ev, "%s is not quieted.", nick, false);
        return 1;
    }

    list_for_each(&quiets, i,q) {
        if(q->user == u && strcmp(q->channel, ev->channel) == 0) {
            irc_mode(q->conn, q->channel, "-q", u->nick);
            list_remove(&quiets, i--);
            break;
        }
    }

    return 0;
}

lsdbot_module_func(init) {
    list_init(&quiets, 8, 8);

    irc_bot_add_command(bot, "quiet", 6, quiet, 3, 3, 2);
    irc_bot_add_command(bot, "unquiet", 6, unquiet, 1, 1, -1);
    irc_bot_add_listener(bot, NULL, "MODE", on_mode, false);

    return 0;
}

lsdbot_module_func(deinit) {
    size_t i;
    struct quiet_ctx *q;

    list_deinit(&quiets);
    irc_bot_rm_command(bot, "quiet");
    irc_bot_rm_command(bot, "unquiet");
    irc_bot_rm_listener(bot, NULL, "MODE", on_mode);

    return 0;
}
